defmodule AsciiArtClientWeb.DrawingsViewTest do
  use AsciiArtClientWeb.ConnCase, async: true

  alias AsciiArtClientWeb.DrawingsView

  describe "display/1" do
    test "return points for a single rectangle" do
      expected = [
        "ctx.fillText(\"@\", 70, 40);",
        "ctx.fillText(\"@\", 70, 30);",
        "ctx.fillText(\"@\", 70, 20);",
        "ctx.fillText(\"@\", 60, 40);",
        "ctx.fillText(\"X\", 60, 30);",
        "ctx.fillText(\"@\", 60, 20);",
        "ctx.fillText(\"@\", 50, 40);",
        "ctx.fillText(\"X\", 50, 30);",
        "ctx.fillText(\"@\", 50, 20);",
        "ctx.fillText(\"@\", 40, 40);",
        "ctx.fillText(\"X\", 40, 30);",
        "ctx.fillText(\"@\", 40, 20);",
        "ctx.fillText(\"@\", 30, 40);",
        "ctx.fillText(\"@\", 30, 30);",
        "ctx.fillText(\"@\", 30, 20);"
      ]

      rectangles = [
        [
          %{"text" => "@", "x" => 3, "y" => 2},
          %{"text" => "@", "x" => 3, "y" => 3},
          %{"text" => "@", "x" => 3, "y" => 4},
          %{"text" => "@", "x" => 4, "y" => 2},
          %{"text" => "X", "x" => 4, "y" => 3},
          %{"text" => "@", "x" => 4, "y" => 4},
          %{"text" => "@", "x" => 5, "y" => 2},
          %{"text" => "X", "x" => 5, "y" => 3},
          %{"text" => "@", "x" => 5, "y" => 4},
          %{"text" => "@", "x" => 6, "y" => 2},
          %{"text" => "X", "x" => 6, "y" => 3},
          %{"text" => "@", "x" => 6, "y" => 4},
          %{"text" => "@", "x" => 7, "y" => 2},
          %{"text" => "@", "x" => 7, "y" => 3},
          %{"text" => "@", "x" => 7, "y" => 4}
        ]
      ]

      result =
        rectangles
        |> DrawingsView.display()
        |> Enum.map(&Phoenix.HTML.safe_to_string(&1))

      assert result == expected
    end

    test "return points for multiple rectangles" do
      expected = [
        "ctx.fillText(\"@\", 140, 120);",
        "ctx.fillText(\"@\", 140, 110);",
        "ctx.fillText(\"@\", 140, 100);",
        "ctx.fillText(\"@\", 130, 120);",
        "ctx.fillText(\"X\", 130, 110);",
        "ctx.fillText(\"@\", 130, 100);",
        "ctx.fillText(\"@\", 120, 120);",
        "ctx.fillText(\"X\", 120, 110);",
        "ctx.fillText(\"@\", 120, 100);",
        "ctx.fillText(\"@\", 110, 120);",
        "ctx.fillText(\"X\", 110, 110);",
        "ctx.fillText(\"@\", 110, 100);",
        "ctx.fillText(\"@\", 100, 120);",
        "ctx.fillText(\"@\", 100, 110);",
        "ctx.fillText(\"@\", 100, 100);",
        "ctx.fillText(\"@\", 40, 20);",
        "ctx.fillText(\"@\", 40, 10);",
        "ctx.fillText(\"@\", 40, 0);",
        "ctx.fillText(\"@\", 30, 20);",
        "ctx.fillText(\"X\", 30, 10);",
        "ctx.fillText(\"@\", 30, 0);",
        "ctx.fillText(\"@\", 20, 20);",
        "ctx.fillText(\"X\", 20, 10);",
        "ctx.fillText(\"@\", 20, 0);",
        "ctx.fillText(\"@\", 10, 20);",
        "ctx.fillText(\"X\", 10, 10);",
        "ctx.fillText(\"@\", 10, 0);",
        "ctx.fillText(\"@\", 0, 20);",
        "ctx.fillText(\"@\", 0, 10);",
        "ctx.fillText(\"@\", 0, 0);"
      ]

      rectangles = [
        [
          %{"text" => "@", "x" => 0, "y" => 0},
          %{"text" => "@", "x" => 0, "y" => 1},
          %{"text" => "@", "x" => 0, "y" => 2},
          %{"text" => "@", "x" => 1, "y" => 0},
          %{"text" => "X", "x" => 1, "y" => 1},
          %{"text" => "@", "x" => 1, "y" => 2},
          %{"text" => "@", "x" => 2, "y" => 0},
          %{"text" => "X", "x" => 2, "y" => 1},
          %{"text" => "@", "x" => 2, "y" => 2},
          %{"text" => "@", "x" => 3, "y" => 0},
          %{"text" => "X", "x" => 3, "y" => 1},
          %{"text" => "@", "x" => 3, "y" => 2},
          %{"text" => "@", "x" => 4, "y" => 0},
          %{"text" => "@", "x" => 4, "y" => 1},
          %{"text" => "@", "x" => 4, "y" => 2}
        ],
        [
          %{"text" => "@", "x" => 10, "y" => 10},
          %{"text" => "@", "x" => 10, "y" => 11},
          %{"text" => "@", "x" => 10, "y" => 12},
          %{"text" => "@", "x" => 11, "y" => 10},
          %{"text" => "X", "x" => 11, "y" => 11},
          %{"text" => "@", "x" => 11, "y" => 12},
          %{"text" => "@", "x" => 12, "y" => 10},
          %{"text" => "X", "x" => 12, "y" => 11},
          %{"text" => "@", "x" => 12, "y" => 12},
          %{"text" => "@", "x" => 13, "y" => 10},
          %{"text" => "X", "x" => 13, "y" => 11},
          %{"text" => "@", "x" => 13, "y" => 12},
          %{"text" => "@", "x" => 14, "y" => 10},
          %{"text" => "@", "x" => 14, "y" => 11},
          %{"text" => "@", "x" => 14, "y" => 12}
        ]
      ]

      result =
        rectangles
        |> DrawingsView.display()
        |> Enum.map(&Phoenix.HTML.safe_to_string(&1))

      assert result == expected
    end

    test "the last rectangle has precedence" do
      expected = [
        "ctx.fillText(\"X\", 10, 40);",
        "ctx.fillText(\"X\", 10, 30);",
        "ctx.fillText(\"@\", 10, 20);"
      ]

      rectangles = [
        [
          %{"x" => 1, "y" => 2, "text" => "@"},
          %{"x" => 1, "y" => 3, "text" => "@"}
        ],
        [
          %{"x" => 1, "y" => 3, "text" => "X"},
          %{"x" => 1, "y" => 4, "text" => "X"}
        ]
      ]

      result =
        rectangles
        |> DrawingsView.display()
        |> Enum.map(&Phoenix.HTML.safe_to_string(&1))

      assert result == expected
    end
  end
end
