defmodule AsciiArtClient.Drawing.Client do
  @moduledoc false

  use Tesla

  plug Tesla.Middleware.BaseUrl, "http://localhost:4000"
  plug Tesla.Middleware.JSON

  def list_canvas() do
    "/canvas"
    |> get()
    |> handle_response()
  end

  def get_canvas(id) do
    ("/canvas/" <> id)
    |> get()
    |> handle_response()
  end

  defp handle_response({:ok, %{status: status} = response})
       when status >= 200 and status <= 399 do
    {:ok, response}
  end

  defp handle_response({:ok, %{status: status}}) when status >= 400 do
    {:error, status}
  end
end
