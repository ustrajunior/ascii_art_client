defmodule AsciiArtClient.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      AsciiArtClientWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: AsciiArtClient.PubSub},
      # Start the Endpoint (http/https)
      AsciiArtClientWeb.Endpoint
      # Start a worker by calling: AsciiArtClient.Worker.start_link(arg)
      # {AsciiArtClient.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: AsciiArtClient.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    AsciiArtClientWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
