defmodule AsciiArtClientWeb.DrawingsController do
  use AsciiArtClientWeb, :controller

  action_fallback AsciiArtClientWeb.FallbackController

  alias AsciiArtClient.Drawing.Client

  def index(conn, _params) do
    with {:ok, %{body: body}} <- Client.list_canvas() do
      render(conn, "index.html", canvas: body)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, %{body: body}} <- Client.get_canvas(id) do
      render(conn, "show.html", canvas: body)
    end
  end
end
