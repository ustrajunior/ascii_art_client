defmodule AsciiArtClientWeb.FallbackController do
  @moduledoc false

  use Phoenix.Controller

  alias AsciiArtClientWeb.ErrorView

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(ErrorView)
    |> render(:"404")
  end

  def call(conn, {:error, status}) do
    conn
    |> put_status(status)
    |> put_view(ErrorView)
    |> render(:"#{status}")
  end
end
