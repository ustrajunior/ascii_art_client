defmodule AsciiArtClientWeb.DrawingsView do
  use AsciiArtClientWeb, :view

  alias Phoenix.HTML

  @scale 10

  def display(rectangles) do
    rectangles
    |> List.flatten()
    |> Enum.reverse()
    |> Enum.uniq_by(fn %{"x" => x, "y" => y} -> %{"x" => x, "y" => y} end)
    |> Enum.map(&draw(&1))
  end

  defp draw(%{"x" => x, "y" => y, "text" => text}) do
    "ctx.fillText(\"#{text}\", #{scale(x)}, #{scale(y)});" |> HTML.raw()
  end

  defp scale(number), do: number * @scale
end
