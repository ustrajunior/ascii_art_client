# Description

This repo is the web client for the AsciiArt project. Its job is to render the points that the backend returns.

I used HTML 5 canvas to display the images, so it's just a matter of setting the x and y coordinates with the text that the app should show at that point.

An index page shows all canvas created, so it's easier to find existing ones.

When overlapping the points, the last image on the list has priority, so only the text for the last image shows on the canvas.

# Examples

![image1](./images/image1.png)
![image2](./images/image2.png)

# How to run

> You need first to start the [backend server ](https://gitlab.com/ustrajunior/ascii_art).

This project is a simple frontend to display images, so there are no databases.
We can use the standard Phoenix instructions to run the server.

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:5000`](http://localhost:5000) from your browser.

The server runs on port `5000` and the backend on port `4000`.

# How to test

Tests use ExUnit so that you can run them with:

```
mix test
```
